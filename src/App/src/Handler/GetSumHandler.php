<?php 

declare(strict_types=1);

namespace App\Handler;

use Laminas\Diactoros\Response\JsonResponse;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

class GetSumHandler implements RequestHandlerInterface
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $requestBody = $request->getParsedBody();

        if (!is_array($requestBody) || !array_key_exists("operands", $requestBody)) {
            return new JsonResponse(['error' => "Invalid request"], 400);
        }

        $sum = 0;

        foreach ($requestBody['operands'] as $value) {
            if (!is_numeric($value)) {
                return new JsonResponse(['error' => "Invalid request"], 400);
            }

            $sum += (int) $value;
        }

        return new JsonResponse(['value' => $sum], 200);
    }
}
